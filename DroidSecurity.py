import os
from threading import Thread
from Serial_Connection import Serial_Connection
from Data_Manager import Data_Manager

at_commands = [
    'AT',
    'AT',
    'AT',
    'AT+CREG=?',
    'at+cmgs=?',
    'ATD=0747433432;'
]



class DroidSecurity():
    def __init__(self):
        print "Droid Security"

    def load_resources(self):
        #open serial conn
        ser = Serial_Connection()
        ser.open_tty()
        self.myCreateThread(ser.read_tty, '', True)

        #test gsm_module rediness
        for cmd in at_commands:
            ser.write_tty(cmd)


        #open Data_Manager
        gsm = Data_Manager()
        self.myCreateThread(gsm.get_json, '', True)



    def myCreateThread (self, function, arg, daemonize):
        try:
            t = Thread(target=function, args = arg )
            t.daemon = daemonize
            t.start()
        except Exception, e:
            print e

if __name__ == "__main__":
    obj = DroidSecurity()
    obj.load_resources()